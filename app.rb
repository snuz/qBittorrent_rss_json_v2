#!/usr/bin/env ruby

require 'yaml'

def os_family
  case RUBY_PLATFORM
  when /ix/i, /ux/i, /gnu/i,
        /sysv/i, /solaris/i,
        /sunos/i, /bsd/i
    'unix'
  when /win/i, /ming/i
    'windows'
  else
    'other'
  end
end

config = YAML.load_file('config.yaml')
quality = config['quality']
feeds = config['feeds']

save_path = if os_family == 'unix'
              #  puts 'linux os '
              config['linux']['path_serial']
            else
              #  puts 'windows os'
              config['windows']['path_serial']
            end

@label = []
@link = []
@hash = []

File.open('file_import.json', 'w:UTF-8') do |f|
  f.write "{\n"

  feeds.each_with_index {|key, value|
    if value == feeds.length - 1
      @hash = [] if @label != key[0]
      @label = key[0]

      key[1].each do |key, value|
        quality = value if key == 'quality'

        @link = value if key == 'link'

        next unless key == 'serial'
        value.each do |x|
          @hash += x.split(',')

          f.write "    \"#{@hash[0]}\"" + ": {\n"
          f.write "        \"addPaused\": null,\n"
          f.write "        \"affectedFeeds\": [\n                \"#{@link}\"\n        ],\n "
          f.write "       \"assignedCategory\": \"Сериалы\",\n"
          f.write "       \"enabled\": true,\n"
          f.write "       \"episodeFilter\": \"\",\n"
          f.write "       \"ignoreDays\": 0,\n"
          f.write "       \"lastMatch\": \"\",\n"
          f.write "       \"mustContain\": \"#{@hash[0]} #{quality}\",\n"
          f.write "       \"mustNotContain\": \"субтитры\",\n"
          f.write "       \"previouslyMatchedEpisodes\": [\n       ],\n"
          f.write "       \"savePath\": \"#{save_path}/#{@hash[1]}\",\n"
          f.write "       \"smartFilter\": false,\n"
          f.write "       \"useRegex\": false\n"
          f.write "    }\n"
        end
      end
    else

      @hash = [] if @label != key[0]

      @label = key[0]

      key[1].each do |key, value|
        quality = value if key == 'quality'

        @link = value if key == 'link'

        next unless key == 'serial'
        value.each do |x|
          @hash = x.split(',')
          f.write "    \"#{@hash[0]}\"" + ": {\n"
          f.write "        \"addPaused\": null,\n"
          f.write "        \"affectedFeeds\": [\n                \"#{@link}\"\n        ],\n "
          f.write "       \"assignedCategory\": \"Сериалы\",\n"
          f.write "       \"enabled\": true,\n"
          f.write "       \"episodeFilter\": \"\",\n"
          f.write "       \"ignoreDays\": 0,\n"
          f.write "       \"lastMatch\": \"\",\n"
          f.write "       \"mustContain\": \"#{@hash[0]} #{quality}\",\n"
          f.write "       \"mustNotContain\": \"субтитры\",\n"
          f.write "       \"previouslyMatchedEpisodes\": [\n       ],\n"
          f.write "       \"savePath\": \"#{save_path}/#{@hash[1]}\",\n"
          f.write "       \"smartFilter\": false,\n"
          f.write "       \"useRegex\": false\n"
          f.write "    },\n"
        end
      end
    end
  }

  f.write '}'
  f.close
end

puts 'complete'
